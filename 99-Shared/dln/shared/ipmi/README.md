e3-ipmimanager
======
ESS Site-specific EPICS module : ipmimanager

# Configuration

The "Details" button contains action with opening window action with macros definitions. The list has all possible macros, if there is no need to define a macro, you can leave it empty.  The macros from cmd file must be transferred to this list. It is important to do it since Jython script checks which macros are defined and based on this, it activates corresponding entries and update fields. 

![Macro definitions](../opi/images/Details_macros.png?raw=true)

For each chassis, you need to create a different set of `SLOTX_IDX` and `SLOTX_MODULE` macros and define `CRATE_NUM`, `P` macro, `MTCA_PREF`, and `IOC_PREF` macros. 

It is essential to type the right `NAME_MODE` value since the Jython scripts configuring panels have implemented logic based on that value.

Remeber to define `IOCNAME` macro as well.

# Usage
This instruction will tell you how to use operator panels to check the basic MTCA parameters.


## Get FRU information and device ID
Open the rack window. Choose the rack and click on "Details". FRU name, slot, and FRU ID have their own columns with update fields.

![FRU information](../opi/images/fru_info.png?raw=true)


## Get hot-swap states
Open the rack window. Choose the rack and click on "Details". You will find information about hot-swap state represented as LED with the label next to the FRU column.

![hot-swap states](../opi/images/hot_swap.png?raw=true)


## Get sensor values
Open the rack window. Choose the rack and click on "Details". Choose the FRU and click on the corresponding "Sensors" button. The new window is composed of the full sensor lists. The yellow border informs about exceeding the non-critical level threshold (lower or upper), red - critical level. threshold.

![Sensor values](../opi/images/sensor_values.png?raw=true)


## Get sensor thresholds levels
![Get thresholds](../opi/images/get_thresh.png?raw=true)


## Set thresholds values
Open the rack window. Choose the rack and click on "Details". Find the needed FRU and click on the corresponding "Sensors" button. Then, choose the sensor and click on the "Details" button. The threshold window will appear. To set new thresholds value use spinners (1). The yellow border starts blinking around "Commit" button if you apply a new value to any of the thresholds levels. When new values are ready to be loaded, click on the "Commit" button (2). After several values, the new thresholds will be loaded to the MCH and the yellow border should stop blinking.

![Set thresholds](../opi/images/set_thresh.png?raw=true)


## Restart FRU
Open the rack window. Choose the rack and click on "Details". Find the FRU that needs to be restarted on the list, click on the "More" button and select the "Expert" window. The new panel will appear with the "Reboot" button. Just click on this button.

![Restart FRU](../opi/images/restart_fru.png?raw=true)


## Restart/Shutdown chassis
Open the rack window. Choose the rack and click on "Details". Find the "Expert" button in the right bottom corner. It will open the new window with buttons to restart and shutdown the chassis.

![Restart chassis](../opi/images/restart_chassis.png?raw=true)

