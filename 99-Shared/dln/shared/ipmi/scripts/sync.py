# This script show info about the need to restart IOC
from org.csstudio.display.builder.runtime.script import PVUtil, ScriptUtil


sync = PVUtil.getInt(pvs[0])

if not sync:
	ScriptUtil.showErrorDialog(widget, "IOC needs to be restarted!")