# This is used to activate labels and arrows when link is active
from org.csstudio.display.builder.runtime.script import PVUtil, ScriptUtil
# To write a portable script, check for the display builder's widget type:
display_builder = 'getVersion' in dir(widget)

if display_builder:  
    phoebus = 'PHOEBUS' in dir(ScriptUtil)
    if phoebus:
	from org.phoebus.framework.macros import Macros
    else:
	from org.csstudio.display.builder.model.macros import Macros
else:
    from org.csstudio.opibuilder.scriptUtil import PVUtil, ConsoleUtil
    ConsoleUtil.writeInfo("Executing in BOY")

import re
from org.csstudio.display.builder.model.properties import WidgetColor


# get display reference
def getDisplay():
	display = widget.getDisplayModel()
	return display

# get display reference
display = getDisplay()
# get macros to generate PVs names
macro = display.getEffectiveMacros()
# macro constructor
macros = Macros()
# get AMC macro value
macro1 = macro.getValue('AMC')
# Green colour meaning active link
color = WidgetColor(61, 216, 61, 255)
# Get string links from PV
links = PVUtil.getStringArray(pvs[0])
# Remove empty elements
links = filter(None, links)
# list for decoded unicode
links_dec = []

# decode from unicode to ascii
for i in links:
	links_dec.append(i.encode('ascii','ignore'))

# join to one string for regex and remove column names
links_str = ' '.join(links_dec[9:-2])
# split string by 's' letter
x = links_str.split("s")

detected = 0
ports = []
for i in x:
	# find amcs
	amc = re.findall(r"(?<=AMC_)[0-9]", i)
	# from list to string
	amc_str = ''.join(amc)
	if amc_str == str(macro1):
		detected = 1
	elif amc_str!=str(macro1) and len(amc_str)>0:
		detected = 0
	if detected:
		# find ports
		ports.append(re.findall(r"(\d)(?:.(?:PCIE|Eth))", i))

# if link is active change label and colour of arrow
for i in ports:
	# from list to string
	x = ''.join(i)
	ScriptUtil.findWidgetByName(display, 'AMC' +  '_Statelabel' + x).setPropertyValue('text', 'Active Link')
	ScriptUtil.findWidgetByName(display, 'AMC' +  '_line' + x).setPropertyValue('line_color', color)
	