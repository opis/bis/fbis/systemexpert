#!/usr/bin/python3

import sys
from pathlib import Path

global str

if len(sys.argv) < 2:
    print("Insert number of records to read")
    exit(1)

num = int(sys.argv[1])

f_bob   = "hb_list.bob"

width  = 800
height =  25

header = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n\
<display version=\"2.0.0\">\n\
<name>list_hb_rec</name>\n\
<width>%i</width>\n\
<height>%i</height>\n"



hb_widget = "<widget type=\"embedded\" version=\"2.0.0\">\n\
<name>Embedded Display_%i</name>\n\
<macros>\n\
<ROW>%i</ROW>\n\
</macros>\n\
<file>hb_record.bob</file>\n\
<y>%i</y>\n\
<width>%i</width>\n\
<height>%i</height>\n\
<resize>2</resize>\n\
</widget>\n"

hb_widgets = []
for i in range(0, num):
   hb_widgets.append(hb_widget % (i,i,i*height,width,height))

bob_w  = header % (width,height*num)
bob_w += "".join([str(i) for i in hb_widgets])
bob_w += "</display>\n"


f = open(f_bob, 'w')
f.write(bob_w)
f.close()
print("\n%s has been written.\n" % f_bob)

